"use strict"

function createNewUser() {
    const user = {
        firstName: "",
        lastName: "",

        set setFirstName(userFirstName) {
            if (userFirstName.length < 1) {
                alert("Value is not valid");

                return;
            }

            Object.defineProperty(user, "firstName", {
                writable: false,
                value: userFirstName,
            });
        },

        set setLastName(userLastName) {
            if (userLastName.length < 1) {
                alert("Value is not valid");

                return;
            }

            Object.defineProperty(user, "lastName", {
                writable: false,
                value: userLastName,
            });
        },

        getLogin() {
            return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
        }
    }

    let userFirstName = prompt("Enter your first name");
    let userLastName = prompt("Enter your last name");

    user.setFirstName = userFirstName;
    user.setLastName = userLastName;

    return user;
}

let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getLogin());


/* Відповіді на теорію:
1. метод об'єкту- це вбудований в об'єкт функції, які працюють в середені об'єкта зі змінними та переданими йому значення - в якості аргументів.
2. Значення властивості об'єкта має такі типи даних: "string", number, true/false та інший object.
3.Об'єкт - це посилальний тип даних- це комбінація трьох значень(object, назва його властовості, та атрибут функції)

 */